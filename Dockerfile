FROM php:8.3-fpm

ARG uid
ARG user

RUN apt-get update && apt-get install -y \
  git \
  curl \
  libpng-dev \
  libonig-dev \
  libxml2-dev \
  libjpeg-dev \
  libfreetype6-dev \
  zip \
  unzip


#installing sendmail
RUN apt-get update && apt-get install -y sendmail

# Copy custom php.ini
COPY php.ini /usr/local/etc/php/

# clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install extensions
RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Create system user to run Composer and artisan commands
RUN useradd -G www-data,root -u $uid -d /home/$user $user
RUN mkdir -p /home/$user/.composer && \
    chown -R $user:$user /home/$user

# Set working directory
WORKDIR /var/www

USER $user

cmd ["php-fpm"]



