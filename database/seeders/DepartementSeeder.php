<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\Service;
use Illuminate\Database\Seeder;

class DepartementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $finance = Department::create([
            'name' => 'Finance Department',
            'description' => 'Manages budgets, taxes, and financial resources.',
        ]);

        $education = Department::create([
            'name' => 'Education Department',
            'description' => 'Supervises schools and educational programs.',
        ]);

        // Créer des services associés
        Service::create([
            'name' => 'Budget Management',
            'description' => 'Planning and monitoring of municipal budgets.',
            'department_id' => $finance->id,
        ]);

        Service::create([
            'name' => 'Tax Collection',
            'description' => 'Management of tax revenues and local taxes.',
            'department_id' => $finance->id,
        ]);

        Service::create([
            'name' => 'School Management',
            'description' => 'Supervision of public educational institutions.',
            'department_id' => $education->id,
        ]);
    }
}
