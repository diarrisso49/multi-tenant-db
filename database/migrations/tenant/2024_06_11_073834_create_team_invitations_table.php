<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('team_invitations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('team_id')->constrained()->cascadeOnDelete();
            $table->string('emails');
            $table->string('csrf_token')->unique();
            $table->unsignedBigInteger('invited_user_id');
            $table->timestamp('accepted_at')->nullable();
            $table->timestamp('expired_at')->useCurrent();
            $table->timestamps();

            $table->unique(['team_id', 'emails', 'invited_user_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('team_invitations');
    }
};
