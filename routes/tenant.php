<?php

declare(strict_types=1);

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\DepartementController;
use App\Http\Controllers\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\Auth\EmailVerificationPromptController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\ProfileController;
use App\Http\Controllers\Auth\ServiceController;
use App\Http\Controllers\Auth\TeamController;
use App\Http\Controllers\Auth\UserController;
use App\Http\Controllers\Auth\VerifyEmailController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TenantContactController;
use App\Http\Controllers\TenantDepartementController;
use App\Http\Controllers\TenantServiceController;
use App\Http\Controllers\TenantTeamController;
use Illuminate\Support\Facades\Route;
use Stancl\Tenancy\Middleware\InitializeTenancyByDomain;
use Stancl\Tenancy\Middleware\PreventAccessFromCentralDomains;

/*
|--------------------------------------------------------------------------
| Tenant Routes
|--------------------------------------------------------------------------
|
| Here you can register the tenant routes for your application.
| These routes are loaded by the TenantRouteServiceProvider.
|
| Feel free to customize them however you want. Good luck!
|
*/
Route::group([
    'as' => 'tenant.',
    'middleware' => [
        'web',
        InitializeTenancyByDomain::class,
        PreventAccessFromCentralDomains::class,
    ]
], static function () {


    Route::get('/login', [AuthenticatedSessionController::class, 'create'])->name('login');
    Route::post('/login', [AuthenticatedSessionController::class, 'store'])->name('store');


    /***
     * tenant routes without middleware
     */

    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::get('/services', [TenantServiceController::class, 'index'])->name('services.index');
    Route::get('/services/{id}', [TenantServiceController::class, 'showServiceDetails'])->name('services.detail');
    Route::get('/team', [TenantTeamController::class, 'index'])->name('team.index');
    Route::get('/contact', [TenantContactController::class, 'index'])->name('contact.index');
    Route::get('/departements', [TenantDepartementController::class, 'index'])->name('departements.index');
    Route::post('/contact', [TenantContactController::class, 'store'])->name('contact.store');

    Route::get('/dashboard', function () {
        return view('dashboard');
    })->middleware(['auth','verified'])->name('dashboard');

    Route::get('/users', [UserController::class, 'index'])->middleware('auth')->name('user.index');
    Route::get('/users/create', [UserController::class, 'create'])->middleware('auth')->name('user.create');
    Route::post('/users', [UserController::class, 'store'])->middleware('auth')->name('user.store');
    Route::get('/users/{user}/edit', [UserController::class, 'edit'])->middleware('auth')->name('user.edit');
    Route::patch('/users/{user}', [UserController::class, 'update'])->middleware('auth')->name('user.update');
    Route::delete('/users/{user}', [UserController::class, 'destroy'])->middleware('auth')->name('user.destroy');


    Route::get('/departement', [DepartementController::class, 'index'])->middleware('auth')->name('departement.index');
    Route::get('/departements/create', [DepartementController::class, 'create'])->middleware('auth')->name('departement.create');
    Route::post('/departements', [DepartementController::class, 'store'])->middleware('auth')->name('departement.store');
    Route::get('/departements/{departement}/edit', [DepartementController::class, 'edit'])->middleware('auth')->name('departement.edit');
    Route::patch('/departements/{departement}', [DepartementController::class, 'update'])->middleware('auth')->name('departement.update');
    Route::delete('/departements/{departement}', [DepartementController::class, 'destroy'])->middleware('auth')->name('departement.destroy');


    Route::get('/service', [ServiceController::class, 'index'])->middleware('auth')->name('service.index');
    Route::get('/services/create', [ServiceController::class, 'create'])->middleware('auth')->name('service.create');
    Route::post('/services', [ServiceController::class, 'store'])->middleware('auth')->name('service.store');
    Route::get('/services/{service}/edit', [ServiceController::class, 'edit'])->middleware('auth')->name('service.edit');
    Route::patch('/services/{service}', [ServiceController::class, 'update'])->middleware('auth')->name('service.update');
    Route::delete('/services/{service}', [ServiceController::class, 'destroy'])->middleware('auth')->name('service.destroy');



    Route::middleware('auth')->group(function () {
        Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
        Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
        Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    });

    Route::middleware('auth')->group(function () {
        Route::get('/teams', [TeamController::class, 'index'])->name('teams.index');
        Route::get('/team/create', [TeamController::class, 'create'])->name('teams.create');
        Route::post('/teams', [TeamController::class, 'store'])->name('teams.store');
        Route::get('/teams/{team}/edit', [TeamController::class, 'edit'])->name('teams.edit');
        Route::patch('/teams/{team}', [TeamController::class, 'update'])->name('teams.update');
        Route::get('/teams/{team}', [TeamController::class, 'show'])->name('teams.show');
        Route::delete('/teams/{team}', [TeamController::class, 'destroy'])->name('teams.destroy');
        Route::post('/teams/add-menbers', [TeamController::class, 'addMember'])->name('teams.addMember');

        Route::post('/teams/{team}/invite', [TeamController::class, 'inviteMember'])->name('teams.invite');
        Route::get('/teams/{team}/accept', [TeamController::class, 'acceptInvitation'])->name('teams.acceptInvitation');
        Route::get('/teamMembers', [TeamController::class, 'getMembersList'])->name('teams.teamMembersList');
    });

    Route::get('/forgot-password', [PasswordResetLinkController::class, 'create'])
        ->middleware('guest')
        ->name('password.request');

    Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
        ->middleware('guest')
        ->name('password.email');

    Route::get('/reset-password/{token}', [NewPasswordController::class, 'create'])
        ->middleware('guest')
        ->name('password.reset');

    Route::post('/reset-password', [NewPasswordController::class, 'store'])
        ->middleware('guest')
        ->name('password.update');

    Route::get('/verify-email', [EmailVerificationPromptController::class, '__invoke'])
        ->middleware('auth')
        ->name('verification.notice');

    Route::get('/verify-email/{id}/{hash}', [VerifyEmailController::class, '__invoke'])
        ->middleware(['signed', 'throttle:6,1'])
        ->name('verification.verify');

    Route::post('/email/verification-notification', [EmailVerificationNotificationController::class, 'store'])
        ->middleware(['auth', 'throttle:6,1'])
        ->name('verification.send');

    Route::get('/confirm-password', [ConfirmablePasswordController::class, 'show'])
        ->middleware('auth')
        ->name('password.confirm');

    Route::post('/confirm-password', [ConfirmablePasswordController::class, 'store'])
        ->middleware('auth');

    Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
        ->middleware('auth')
        ->name('logout');
});
