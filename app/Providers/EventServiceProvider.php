<?php

namespace App\Providers;


use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;


class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
       /* DatabaseBusy::class => [
            DatabaseApproachingMaxConnections::class,
        ],*/
    ];

    /**
     * Register any events for your application.
     */
    public function boot(): void
    {
//        Event::listen(function (DatabaseBusy $event) {
//            Notification::route('mail', 'dev@example.com')
//                ->notify(new DatabaseApproachingMaxConnections(
//                    $event->connectionName,
//                    $event->connections
//                ));
//        });
    }

}
