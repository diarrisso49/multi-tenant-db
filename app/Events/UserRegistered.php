<?php

namespace App\Events;

use App\Models\User;
use Illuminate\Queue\SerializesModels;

class UserRegistered
{
    use  SerializesModels;

    /**
     * Create a new event instance.
     */
    public function __construct(private User $user){}


}
