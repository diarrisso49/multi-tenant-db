<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function sendTestEmail()
    {
        Mail::raw('This is a test email sent using MailHog.', function ($message) {
            $message->to('your-email@example.com')
                ->subject('Test Email');
        });

        return response()->json(['message' => 'Test email sent successfully!']);
    }

}
