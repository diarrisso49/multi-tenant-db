<?php

namespace App\Http\Controllers;

use App\Models\Service;

class TenantServiceController extends Controller
{


    public function index()
    {
        $services = Service::with('department')->get();

        return view('tenant.service.index', compact('services'));
    }


    public function showServiceDetails($id): Vie
    {
        $service = Service::with('department')->findOrFail($id);
        return view('tenant.service.show', compact('service'));

    }

}
