<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserRegistered;
use App\Models\Team;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;

class UserController
{

    public function index()
    {
        $users = User::where('tenant_id', tenant('id'))
            ->select('name', 'emails', 'tenant_id', 'is_admin')
            ->get();

        return view('users.index', compact('users'));
    }

    public function create()
    {
        $tenants = tenant('id');
        $teams = Team::all();

        return view('users.create', compact('teams', 'tenants'));
    }

    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'emails' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);


        $user = User::create([
            'name' => $request->name,
            'firstname' => $request->firstname,
            'emails' => $request->emails,
            'position' => $request->position,
            'password' => bcrypt($request->password),
            'tenant_id' => tenant('id'),
            'current_team_id' => $request->current_team_id,
        ]);

        //event(new UserRegistered($user));
        //event(new Registered($user));

        return redirect()->route('tenant.user.index');
    }


    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required',
            'emails' => 'required|email|unique:users,emails,' . $user->id,
        ]);

        $user->update($request->all());

        return redirect()->route('users.index');
    }

    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('users.index');
    }

    public function sendEmail(): void
    {

        $to = 'diarrisso49@gmail.com';
        $subject = 'Salut teste';
        $message = 'Salut teste';
        $headers = 'From: your-email@example.com' . "\r\n" .
            'Reply-To: your-email@example.com' . "\r\n" .
            'X-Mailer: PHP/' . PHP_VERSION;

        if (mail($to, $subject, $message, $headers)) {
            echo 'Email sent successfully.';
        } else {
            echo 'Failed to send email.';
        }
        // $user->notify(new \App\Notifications\CustomVerifyEmailQueued());
    }

}
