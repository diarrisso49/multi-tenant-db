<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Mail\TeamInvitations;
use App\Models\Department;
use App\Models\Team;
use App\Models\TeamInvitation;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $teams = Team::with('users')->get();
        $selectedTeams = Team::pluck('name', 'id')->toArray();
        return view('auth.teams.index', compact('teams', 'selectedTeams'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

        $teams = Team::all();
        $departements = Department::pluck('name', 'id')->toArray();
        return view('auth.teams.create', compact('teams', 'departements'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {


        $validatData = $request->validate([
            'name' => 'required|string|max:255',
            'personal_team' => 'required|boolean',
            'team_owner' => 'required|string|max:255',
            'departement_id' => 'required|exists:departements,id',

       ]);



        $team = Team::create([
            'name' => $validatData['name'],
            'personal_team' => $validatData['personal_team'],
            'team_owner' => Auth::user()->name,
            'tenant_id' => tenant('id'),
            'departement_id' => $validatData['departement_id']
        ]);


        $user = User::find(1);

        $team->users()->attach($user->id);

        return redirect()->route('tenant.teams.index');

    }

    /**
     * Display the specified resource.
     */
    public function show(Team $team)
    {
        return view('auth.teams.show', ['team' => Team::findOrFail($team)]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        return view('auth.teams.edit', ['team' => Team::findOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update( Request $request, Team $team, )
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'personal_team' => 'required|boolean',
            'team_owner' => 'required|string|max:255',
        ]);

        $team->update([
            'name' => $request->name,
            'personal_team' => $request->personal_team,
            'team_owner' => $request->team_owner,
        ]);

        return redirect()->route('tenant.teams.index');
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Team $team): \Illuminate\Http\RedirectResponse
    {
        $team->delete();
        return redirect()->route('tenant.teams.index');
    }

    /**
     * @throws AuthorizationException
     */
    public function addMember(Request $request, Team $team, User $user): \Illuminate\Http\RedirectResponse
    {
        $this->authorize('manageMembers', $team);

        $request->validate([
            'user_id' => 'required|exists:users,id',
            'role_names' => 'required|array',
            'role_names.*' => 'exists:roles,name',
        ]);

         $user->findOrFail($request->user_id);

        $team->members()->attach($user->id);

        foreach ($request->role_names as $roleName) {
            $team->assignRoleToMember($user, $roleName);
        }

        $team->users()->attach($user->id);

        return redirect()->route('tenant.teams.index', ['team' => $team]);
    }

    public function removeMember(Team $team, User $user)
    {

        $this->authorize('manageMembers', $team);
        $team->users()->detach($user->id);
        return redirect()->route('tenant.teams.index', ['team' => $team]);
    }


    public function inviteMember(Request $request): \Illuminate\Http\RedirectResponse
    {
       // $this->authorize('manageMembers', $team);
        try {
            $request->validate([
                'email' => 'required|email|exists:users,email',
            ]);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        $user = User::where('emails', $request->emails)->firstOrFail();
        $team = Team::findOrFail($request->input('team_id'));

        Mail::to($user->emails)->send(new TeamInvitations($team, Auth::user()));

        return redirect()->route('tenant.teams.index', $team->id)->with('status', 'Invitation envoyée.');
    }


    public function acceptInvitation(Request $request, Team $team): \Illuminate\Http\RedirectResponse
    {
        $user = Auth::user();

        if (!$team->members->contains($user) ) {

            if ($user instanceof User) {
                TeamInvitation::Create([
                    'team_id' => $team->id,
                    'emails' => $user->emails,
                    'invited_user_id' => $user->id ,
                    'csrf_token' => Str::random(40),
                    'expired_at' => now()->addMinutes(60),
                    'accepted_at' => now(),

                ]);
            }

            $team->members()->attach($user->id);
        }

        return redirect()->route('tenant.teams.index', $team->id)->with('status', 'Vous avez rejoint l\'équipe.');
    }


    public function rejectInvitation(Request $request, Team $team): \Illuminate\Http\RedirectResponse
    {

        return redirect()->route('tenant.teams.index', $team->id)->with('status', 'Vous avez rejeté l\'invitation.');
    }

    public function cancelInvitation(Request $request, Team $team): \Illuminate\Http\RedirectResponse
    {

        return redirect()->route('tenant.teams.index', $team->id)->with('status', 'Vous avez annulé l\'invitation.');
    }


    public function getMembersList()
    {
        TeamInvitation::all();
        return view('teams.members');
    }
}
