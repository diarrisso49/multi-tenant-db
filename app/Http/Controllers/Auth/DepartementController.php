<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\StoreDepartementRequest;
use App\Models\Department;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class DepartementController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $departements = Department::all();
        return view('auth.departements.index', compact('departements'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('auth.departements.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreDepartementRequest $request): RedirectResponse
    {

        $departement = Department::create($request->validated());


        return redirect()->route('tenant.departement.index')->with('success', 'Department created successfully');

    }

    /**
     * Display the specified resource.
     */
    public function show(Department $departement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Department $departement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Department $departement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Department $departement)
    {
        //
    }
}
