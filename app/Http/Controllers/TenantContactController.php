<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use App\Mail\ContactMail;
use App\Models\Tenant;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class TenantContactController extends Controller
{

    public function index(): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('tenant.contact.create');
    }


    public function store(ContactRequest $request): bool|string
    {
        try {

            $validations  = $request->validated();

            $contact = [
                'name' => $validations['name'],
                'email' => $validations['email'],
                'message' => $validations['message'],
                'tenant_id' => tenant('id')
            ];

            $contact = Contact::create($contact);

            Mail::to($validations['email'])->send(new ContactMail($contact));

            return redirect()->route('tenant.home')->with('success', 'Message sent successfully');

        } catch (\Exception $e) {

            return redirect()->back()->with('error', 'An error occurred while sending message');
        }
    }
}
