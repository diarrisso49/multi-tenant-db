<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisteredTenantRequest;
use App\Models\Domain;
use App\Models\Tenant;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;

class RegisteredTenantController extends Controller
{

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('auth.register');

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(RegisteredTenantRequest $request)
    {

        $validations  = $request->validated();


        $tenant = Tenant::create([

            'id' => $request->company,
            'name' => $validations['company'],
            'firstname' => $validations['firstname'],
            'position' => $validations['position'],
            'domain' => $validations['domain'],
            'emails' => $validations['emails'],
            'password' => $validations['password'],
        ]);


        /*$tenant->createTeam([
            'name' => $validations['company'],
            'personal_team' => true,
        ]);*/


        $tenant->createDomain(['domain' => $request->domain]);



        // Fire the registered event
        event(new Registered($tenant));


        return redirect( tenant_route($tenant->domains->first()->domain, 'tenant.login'));
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
