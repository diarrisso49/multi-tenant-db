<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class StoreServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|unique:services|string|max:25',
            'description' => 'required|string|max:255',
            'manager' => 'required|string|max:25',
            'departement_id' => 'required|exists:departements,id',
            'team_id' => 'required|exists:teams,id',
            'tenant_id' => 'required',
        ];
    }

    public function prepareForValidation(): void
    {
        $this->merge([
            'tenant_id' => tenant('id'),
        ]);
    }
}
