<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Auth\EmailVerificationRequest as BaseEmailVerificationRequest;
use JetBrains\PhpStorm\NoReturn;

class EmailVerificationRequest extends BaseEmailVerificationRequest
{
    protected $user;

    public function authorize()
    {
        $this->user = \App\Models\User::find($this->route('id'));
        if ($this->user != null) {
            if (!hash_equals((string)$this->route('id'),
                (string)$this->user->getKey())) {
                return false;
            }

            if (!hash_equals((string)$this->route('hash'),
                sha1($this->user->getEmailForVerification()))) {
                return false;
            }

            return true;
        }

        return false;
    }

    #[NoReturn] #[NoReturn] protected function prepareForValidation(): void
    {
        $user = User::findOrFail($this->route('id'));
        $this->setUserResolver(function () use ($user) {
            return $user;
        });

    }


}
