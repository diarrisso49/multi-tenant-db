<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:25',
            'is_admin' => 'required|boolean',
            'emails' => 'required|string|email|max:25|unique:users',
            'tenant_id' => 'required|string|max:25',
            'position' => 'required|string|max:25',
            'firstname' => 'required|string|max:25',
            'password' => 'required|string|min:8',
        ];
    }

    public function messages(): array
    {
        return [
            'name.required' => 'Name is required',
            'emails.required' => 'Email is required',
            'tenant_id.required' => 'Tenant is required',
            'position.required' => 'Position is required',
            'firstname.required' => 'Firstname is required',
            'password.required' => 'Password is required',
        ];
    }


    public function prepareForValidation(): void
    {
        $this->merge([
            'tenant_id' => tenant('id'),
        ]);
    }
}
