<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;


class TeamInvitation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'emails',
        'team_id',
        'token',
        'accepted_at',
        'expired_at',
        'invited_user_id'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($invitation) {
            $invitation->csrf_token = Str::random(40);
            $invitation->expired_at = Carbon::now()->addMinutes(60);
        });
    }

    /**
     * Get the team that the invitation belongs to.
     *
     * @return BelongsTo
     */
    public function team(): BelongsTo
    {
        return $this->belongsTo(Team::class, 'team_id', 'id');
    }

    public function invitedUser()
    {
        return $this->belongsTo(User::class, 'invited_user_id', 'id');

    }
}
