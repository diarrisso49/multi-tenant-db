<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'personal_team' => 'boolean',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'personal_team',
        'team_owner',
        'tenant_id',
        'private',
        'departement_id',
    ];






    public function tenant()
    {
        return $this->belongsTo(Tenant::class, 'tenant_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'team_user');
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'team_owner');
    }

    public function members()
    {
        return $this->belongsToMany(User::class, 'team_user');
    }


    public function roles()
    {
        return $this->belongsToMany(Role::class, 'model_has_roles', 'team_id', 'role_id')
            ->wherePivot('model_type', User::class);
    }

    public function assignRoleToMember(User $user, $roleName)
    {
        $user->assignRole($roleName, $this);
    }

    public function removeRoleFromMember(User $user, $roleName)
    {
        $user->removeRole($roleName, $this);
    }

   /* public static function boot(): void
    {
        parent::boot();

        // here assign this team to a global user with global default role
        self::created(static function ($model) {
            // temporary: get session team_id for restore at end
            $session_team_id = getPermissionsTeamId();
            // set actual new team_id to package instance
            setPermissionsTeamId($model);
            // get the admin user and assign roles/permissions on new team model
            User::find(Auth::id())->assignRole('Super Admin');
            // restore session team_id to package instance using temporary value stored above
            setPermissionsTeamId($session_team_id);
        });
    }*/

    public function departement()
    {
        return $this->belongsTo(Department::class);
    }

    public function services()
    {
        return $this->hasMany(Service::class);
    }
}
