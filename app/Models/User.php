<?php

namespace App\Models;

use App\Notifications\CustomVerifyEmailQueued;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Notifications\Notification;


class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;



    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'emails',
        'password',
        'tenant_id',
        'is_admin',
        'position',
        'firstname',


    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];


    public function sendEmailVerificationNotification(): void
    {
        $this->notify(new CustomVerifyEmailQueued());
    }


    public function routeNotificationForMail(Notification $notification): array|string
    {
        return [$this->emails => $this->name];
    }

    public function teams(): BelongsToMany
    {
        return $this->belongsToMany(Team::class, 'team_user')->withTimestamps();
    }

    public function tenant(): BelongsTo
    {
        return $this->belongsTo(Tenant::class, 'tenant_id');
    }


    public function isOwner(Team $team): bool
    {
        return $this->id === $team->team_owner;
    }

    public function hasRoleWithinTeam($roleName, Team $team)
    {
        return $this->roles()->where('name', $roleName)->wherePivot('team_id', $team->id)->exists();
    }

    public function getEmailForVerification(): string
    {
        return $this->emails;
    }


    public function isAdmin()
    {
        return $this->is_admin == 1;
    }

    public function isSuperAdmin()
    {
        return $this->is_admin == 2;
    }

    public function services()
    {
        return $this->belongsToMany(Service::class, 'service_user', 'user_id', 'service_id');
    }


    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class);
    }
}

