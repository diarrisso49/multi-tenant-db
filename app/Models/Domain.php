<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Models\Domain as ModelDomain;

class Domain extends ModelDomain
{
    use HasFactory;

}
