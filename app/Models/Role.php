<?php

namespace App\Models;

use Spatie\Permission\Models\Role as SpatieRole;

class Role extends SpatieRole
{


    public function teams()
    {
        return $this->belongsToMany(Team::class, 'role_team');
    }


    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }
}
