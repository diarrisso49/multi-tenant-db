<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description', 'manager', 'department_id', 'team_id', 'tenant_id'];


    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function contacts()
    {
        return $this->hasMany(Contact::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'service_user', 'service_id', 'user_id');
    }
}
