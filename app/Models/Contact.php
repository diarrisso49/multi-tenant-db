<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;


    protected $fillable = ['name', 'email', 'message', 'tenant_id'];


    public function tenant(): void
    {
        $this->belongsTo(Tenant::class);
    }

    public function service(): void
    {
        $this->hasMany(Service::class, 'service_id');
    }
}
