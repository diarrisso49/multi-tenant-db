<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailVerification
{

    /**
     * Handle the event.
     */
    public function handle(UserRegistered $event): void
    {
        $event->$this->user->sendEmailVerificationNotification();
    }
}
