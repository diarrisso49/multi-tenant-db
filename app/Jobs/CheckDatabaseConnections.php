<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Events\DatabaseBusy;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CheckDatabaseConnections implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        try {
            $connectionName = config('database.default');
            $currentConnections = DB::select('SHOW STATUS LIKE "Threads_connected"')[0]->Value;
            $maxConnections = config('database.connections.' . $connectionName . '.max_connections');

            Log::info('Checking database connections', [
                'connectionName' => $connectionName,
                'currentConnections' => $currentConnections,
                'maxConnections' => $maxConnections,
            ]);

            if ($currentConnections >= $maxConnections) {
                event(new DatabaseBusy($connectionName, $currentConnections));
            }
        } catch (\Exception $e) {
            Log::error('Failed to check database connections', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            ]);
        }
    }
}
