@if ($message = Session::get('success'))

    <div id="success"
         class=" flash-message p-4 mb-4 text-sm text-green-800 rounded-lg bg-green-50 dark:bg-gray-800 dark:text-green-400"
         role="alert">
        {{ $message }}
    </div>

@endif
@if ($message = Session::get('error'))
    <div class=" flash-message p-4 mb-4 text-sm text-red-800 rounded-lg bg-red-50 dark:bg-gray-800 dark:text-red-400"
         role="alert">
        {{ $message }}
    </div>

@endif
@if ($message = Session::get('warning'))

    <div
        class=" flash-message p-4 mb-4 text-sm text-yellow-800 rounded-lg bg-yellow-50 dark:bg-gray-800 dark:text-yellow-300"
        role="alert">
        <strong>{{ $message }}</strong>
    </div>
@endif
@if ($message = Session::get('info'))
    <div class=" flash-message p-4 mb-4 text-sm text-blue-800 rounded-lg bg-blue-50 dark:bg-gray-800 dark:text-blue-400"
         role="alert">
        <strong>{{ $message }}</strong>
    </div>
@endif
@if ($errors->any())
    <div class="flex items-center p-4 mb-4 text-blue-800 rounded-lg bg-blue-50 dark:bg-gray-800 dark:text-blue-400"
         role="alert">
        Please check the form below for errors
    </div>
@endif


