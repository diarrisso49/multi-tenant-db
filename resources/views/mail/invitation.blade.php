<x-mail::message>
     {{ $team->name }}

    Hello {{ $invitedUser->name }},

    Vous avez été invité à rejoindre l'équipe "{{ $team->name }}" sur notre application.


<x-mail::button :url="route('tenant.teams.acceptInvitation', $team->id)">
 join the team
</x-mail::button>

Thanks,<br>
{{ config('app.name') }}
</x-mail::message>





