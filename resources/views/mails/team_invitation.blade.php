@component('mail::message')
    # Invitation à rejoindre l'équipe {{ $team->name }}

    Bonjour {{ $invitedUser->name }},

    Vous avez été invité à rejoindre l'équipe "{{ $team->name }}" sur notre application.

    @component('mail::button', ['url' => route('tenant.teams.acceptInvitation', $team->id)])
        Rejoindre l'équipe
    @endcomponent

    Merci,<br>
    {{ tenant('id') }}
@endcomponent
