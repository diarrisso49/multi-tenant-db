<x-mail::message>
    {{ $name }}

    Thanks,<br>
    {{ config('app.name') }}
</x-mail::message>

