
@props(['options', 'selected' => null])


<select {{ $attributes }} class="border-gray-300 dark:border-gray-700 dark:bg-gray-900 dark:text-gray-300 focus:border-indigo-500 dark:focus:border-indigo-600 focus:ring-indigo-500 dark:focus:ring-indigo-600 rounded-md shadow-sm">
    @foreach ($options as $key => $value)
    <option class="dark:active:bg-gray-300" value="{{ $key }}" {{ $selected === $key ? 'selected' : '' }}>
    {{ $value }}
    </option>
    @endforeach
</select>

