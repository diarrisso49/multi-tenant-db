
<span class="alert-title">{{ $title }}</span>

<div class="alert alert-{{ $type }}">
    {{ $slot }}
</div>


<div class="alert alert-danger">
    {{ $slot }}
</div>


@props(['type' => 'info', 'message'])

<div {{ $attributes->merge(['class' => 'alert alert-'.$type]) }}>
    {{ $message }}
</div>


<x-alert type="error" :message="$message" class="mb-4"/>
