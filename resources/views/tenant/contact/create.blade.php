<x-tenants-layout>

    <!-- Contact Section -->
    <section class="container mx-auto p-6 mt-6">
        <h2 class="text-3xl font-semibold mb-4 text-center">Contact Us</h2>


        {{--  @if ($errors->any())
              <div class="text-red-600">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif--}}
        <div class="bg-white shadow-md rounded-lg p-6">
            <form action="{{ route('tenant.contact.store') }}" method="POST">
                @csrf
                <div class="mb-4">
                    <label for="name" class="block text-gray-700">Name</label>
                    <input
                        type="text"
                        id="name"
                        name="name"
                        value="{{ old('name') }}"
                        class="@error('name') border-red-700 @enderror w-full px-4 py-2 border rounded-lg focus:outline-none focus:ring-2 focus:ring-blue-600"
                    >

                    @error('name')
                    <div class="text-red-700">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mb-4">
                    <label for="email" class="block text-gray-700">Email</label>
                    <input
                        type="email"
                        id="email" name="email"
                        value="{{ old('email') }}"
                        class=" @error('email')  border-red-700 @enderror w-full px-4 py-2 border rounded-lg focus:outline-none focus:ring-2 focus:ring-blue-600"
                    >

                    @error('email')
                    <div class="text-red-700">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mb-4">
                    <label for="message" class="block text-gray-700">Message</label>
                    <textarea
                        id="message"
                        name="message"
                        class="@error('message') border-red-700 @enderror w-full px-4 py-2 border rounded-lg focus:outline-none focus:ring-2 focus:ring-blue-600"
                        rows="4"
                    >
                        {{ old('message') }}
                    </textarea>

                    @error('message')
                    <div class="text-red-700">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="w-full bg-blue-800 text-white py-2 rounded-lg hover:bg-blue-700">Send Message</button>
            </form>
        </div>
    </section>


</x-tenants-layout>
