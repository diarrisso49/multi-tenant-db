<x-tenants-layout>

    <section class="container mx-auto my-12 px-4">
        <h2 class="text-2xl font-semibold text-blue-600 mb-8">Nos départements</h2>
        <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
            <!-- Département 1 -->
            <div class="bg-white p-6 rounded-lg shadow-lg">
                <h3 class="text-xl font-semibold text-blue-600 mb-4">Département des Finances</h3>
                <p class="text-gray-700 leading-relaxed">
                    Gestion des budgets, des impôts et des ressources financières de la commune.
                </p>
            </div>

            <!-- Département 2 -->
            <div class="bg-white p-6 rounded-lg shadow-lg">
                <h3 class="text-xl font-semibold text-blue-600 mb-4">Département de l'Éducation</h3>
                <p class="text-gray-700 leading-relaxed">
                    Supervision des écoles, des programmes éducatifs et des activités parascolaires.
                </p>
            </div>

            <!-- Département 3 -->
            <div class="bg-white p-6 rounded-lg shadow-lg">
                <h3 class="text-xl font-semibold text-blue-600 mb-4">Département des Travaux Publics</h3>
                <p class="text-gray-700 leading-relaxed">
                    Entretien des routes, des ponts et des infrastructures publiques.
                </p>
            </div>

            <!-- Département 4 -->
            <div class="bg-white p-6 rounded-lg shadow-lg">
                <h3 class="text-xl font-semibold text-blue-600 mb-4">Département de la Santé</h3>
                <p class="text-gray-700 leading-relaxed">
                    Gestion des hôpitaux, des cliniques et des programmes de santé publique.
                </p>
            </div>

            <!-- Département 5 -->
            <div class="bg-white p-6 rounded-lg shadow-lg">
                <h3 class="text-xl font-semibold text-blue-600 mb-4">Département de l'Environnement</h3>
                <p class="text-gray-700 leading-relaxed">
                    Protection de l'environnement, gestion des déchets et des espaces verts.
                </p>
            </div>

            <!-- Département 6 -->
            <div class="bg-white p-6 rounded-lg shadow-lg">
                <h3 class="text-xl font-semibold text-blue-600 mb-4">Département de la Culture</h3>
                <p class="text-gray-700 leading-relaxed">
                    Promotion des activités culturelles, des musées et des événements artistiques.
                </p>
            </div>
        </div>
    </section>


    <!-- Department List -->
    <section class="container mx-auto my-12 px-4">
        <h2 class="text-2xl font-semibold text-blue-600 mb-8">Our Departments</h2>
        <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
            <!-- Department 1 -->
            <div class="bg-white p-6 rounded-lg shadow-lg">
                <h3 class="text-xl font-semibold text-blue-600 mb-4">Finance Department</h3>
                <p class="text-gray-700 leading-relaxed">
                    Management of budgets, taxes, and financial resources for the municipality.
                </p>
            </div>

            <!-- Department 2 -->
            <div class="bg-white p-6 rounded-lg shadow-lg">
                <h3 class="text-xl font-semibold text-blue-600 mb-4">Education Department</h3>
                <p class="text-gray-700 leading-relaxed">
                    Supervision of schools, educational programs, and extracurricular activities.
                </p>
            </div>

            <!-- Department 3 -->
            <div class="bg-white p-6 rounded-lg shadow-lg">
                <h3 class="text-xl font-semibold text-blue-600 mb-4">Public Works Department</h3>
                <p class="text-gray-700 leading-relaxed">
                    Maintenance of roads, bridges, and public infrastructure.
                </p>
            </div>

            <!-- Department 4 -->
            <div class="bg-white p-6 rounded-lg shadow-lg">
                <h3 class="text-xl font-semibold text-blue-600 mb-4">Health Department</h3>
                <p class="text-gray-700 leading-relaxed">
                    Management of hospitals, clinics, and public health programs.
                </p>
            </div>

            <!-- Department 5 -->
            <div class="bg-white p-6 rounded-lg shadow-lg">
                <h3 class="text-xl font-semibold text-blue-600 mb-4">Environment Department</h3>
                <p class="text-gray-700 leading-relaxed">
                    Environmental protection, waste management, and green spaces.
                </p>
            </div>

            <!-- Department 6 -->
            <div class="bg-white p-6 rounded-lg shadow-lg">
                <h3 class="text-xl font-semibold text-blue-600 mb-4">Culture Department</h3>
                <p class="text-gray-700 leading-relaxed">
                    Promotion of cultural activities, museums, and artistic events.
                </p>
            </div>
        </div>
    </section>


</x-tenants-layout>
