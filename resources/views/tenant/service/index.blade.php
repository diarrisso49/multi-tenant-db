<x-tenants-layout>
    <!-- Main Content -->
    <main class="container mx-auto my-12 px-4">
        <!-- Service Grid -->
        <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
            <!-- Service 1 -->
            <div class="bg-white p-6 rounded-lg shadow-lg">
                <span class="inline-block bg-blue-100 text-blue-800 text-sm font-semibold px-3 py-1 rounded-full mb-2">Finance Department</span>
                <h3 class="text-xl font-semibold text-blue-600 mb-2">Budget Management</h3>
                <p class="text-gray-700">Planning and monitoring of municipal budgets.</p>
            </div>

            <!-- Service 2 -->
            <div class="bg-white p-6 rounded-lg shadow-lg">
                <span class="inline-block bg-blue-100 text-blue-800 text-sm font-semibold px-3 py-1 rounded-full mb-2">Finance Department</span>
                <h3 class="text-xl font-semibold text-blue-600 mb-2">Tax Collection</h3>
                <p class="text-gray-700">Management of tax revenues and local taxes.</p>
            </div>

            <!-- Service 3 -->
            <div class="bg-white p-6 rounded-lg shadow-lg">
                <span class="inline-block bg-blue-100 text-blue-800 text-sm font-semibold px-3 py-1 rounded-full mb-2">Education Department</span>
                <h3 class="text-xl font-semibold text-blue-600 mb-2">School Management</h3>
                <p class="text-gray-700">Supervision of public educational institutions.</p>
            </div>

            <!-- Service 4 -->
            <div class="bg-white p-6 rounded-lg shadow-lg">
                <span class="inline-block bg-blue-100 text-blue-800 text-sm font-semibold px-3 py-1 rounded-full mb-2">Education Department</span>
                <h3 class="text-xl font-semibold text-blue-600 mb-2">Extracurricular Activities</h3>
                <p class="text-gray-700">Organization of educational and cultural events.</p>
            </div>

            <!-- Service 5 -->
            <div class="bg-white p-6 rounded-lg shadow-lg">
                <span class="inline-block bg-blue-100 text-blue-800 text-sm font-semibold px-3 py-1 rounded-full mb-2">Public Works Department</span>
                <h3 class="text-xl font-semibold text-blue-600 mb-2">Road Maintenance</h3>
                <p class="text-gray-700">Maintenance of public roads and infrastructure.</p>
            </div>

            <!-- Service 6 -->
            <div class="bg-white p-6 rounded-lg shadow-lg">
                <span class="inline-block bg-blue-100 text-blue-800 text-sm font-semibold px-3 py-1 rounded-full mb-2">Public Works Department</span>
                <h3 class="text-xl font-semibold text-blue-600 mb-2">Public Lighting</h3>
                <p class="text-gray-700">Installation and maintenance of public lighting.</p>
            </div>

            <!-- Service 7 -->
            <div class="bg-white p-6 rounded-lg shadow-lg">
                <span class="inline-block bg-blue-100 text-blue-800 text-sm font-semibold px-3 py-1 rounded-full mb-2">Health Department</span>
                <h3 class="text-xl font-semibold text-blue-600 mb-2">Hospital Management</h3>
                <p class="text-gray-700">Supervision of public healthcare facilities.</p>
            </div>

            <!-- Service 8 -->
            <div class="bg-white p-6 rounded-lg shadow-lg">
                <span class="inline-block bg-blue-100 text-blue-800 text-sm font-semibold px-3 py-1 rounded-full mb-2">Health Department</span>
                <h3 class="text-xl font-semibold text-blue-600 mb-2">Vaccination Programs</h3>
                <p class="text-gray-700">Prevention and vaccination campaigns.</p>
            </div>

            <!-- Service 9 -->
            <div class="bg-white p-6 rounded-lg shadow-lg">
                <span class="inline-block bg-blue-100 text-blue-800 text-sm font-semibold px-3 py-1 rounded-full mb-2">Environment Department</span>
                <h3 class="text-xl font-semibold text-blue-600 mb-2">Waste Management</h3>
                <p class="text-gray-700">Collection and recycling of municipal waste.</p>
            </div>
        </div>
    </main>


</x-tenants-layout>
