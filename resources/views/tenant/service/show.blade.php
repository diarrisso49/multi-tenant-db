<x-tenants-layout>
    <!-- Main Content -->
    <main class="container mx-auto my-12 px-4">
        <!-- Service Details -->
        <div class="bg-white p-8 rounded-lg shadow-lg">
            <h2 class="text-2xl font-semibold text-blue-600 mb-4">About This Service</h2>
            <p class="text-gray-700 leading-relaxed mb-6">
                The Budget Management service is responsible for planning, monitoring, and controlling the
                municipality's financial resources.
                This includes the preparation of annual budgets, tracking expenditures, and ensuring compliance with
                financial regulations.
            </p>

            <h3 class="text-xl font-semibold text-blue-600 mb-2">Key Responsibilities</h3>
            <ul class="list-disc list-inside text-gray-700 mb-6">
                <li>Preparation of annual budgets</li>
                <li>Monitoring of expenditures</li>
                <li>Financial reporting and analysis</li>
                <li>Compliance with financial regulations</li>
            </ul>

            <h3 class="text-xl font-semibold text-blue-600 mb-2">Contact Information</h3>
            <p class="text-gray-700 mb-2"><strong>Email:</strong> budget@municipality.com</p>
            <p class="text-gray-700"><strong>Phone:</strong> +123 456 7890</p>
        </div>
    </main>
</x-tenants-layout>
