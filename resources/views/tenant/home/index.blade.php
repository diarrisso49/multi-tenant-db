<x-tenants-layout>
    <section id="about" class="bg-gray-200 py-16">
        <div class="container mx-auto px-4">
            <h2 class="text-3xl font-bold text-center mb-12">À propos de nous</h2>
            <div class="grid grid-cols-1 md:grid-cols-2 gap-8">
                <div>
                    <p class="text-gray-700 mb-4">
                        MonEntreprise est une entreprise innovante spécialisée dans le conseil, le développement web et
                        la formation.
                        Nous aidons nos clients à atteindre leurs objectifs grâce à des solutions sur mesure et une
                        expertise de pointe.
                    </p>
                    <p class="text-gray-700">
                        Notre équipe est composée de professionnels passionnés, prêts à vous accompagner dans tous vos
                        projets.
                    </p>
                </div>
                <div class="flex justify-center">
                    <img src="https://via.placeholder.com/400" alt="À propos de nous" class="rounded-lg shadow-lg">
                </div>
            </div>
        </div>
    </section>

</x-tenants-layout>
