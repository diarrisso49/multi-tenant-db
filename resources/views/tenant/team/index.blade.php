<x-tenants-layout>
    <!-- Team Section -->
    <section class="container mx-auto p-6 mt-6">
        <h2 class="text-3xl font-semibold mb-4 text-center">Meet the Team</h2>
        <div class="grid md:grid-cols-3 gap-6">

            <div class="bg-white shadow-md rounded-lg p-6 text-center">
                <img src="https://via.placeholder.com/150" alt="Team Member 1" class="rounded-full mx-auto mb-4">
                <h3 class="text-xl font-semibold mb-2">John Doe</h3>
                <p class="text-gray-600">Director of Operations</p>
            </div>
            <div class="bg-white shadow-md rounded-lg p-6 text-center">
                <img src="https://via.placeholder.com/150" alt="Team Member 2" class="rounded-full mx-auto mb-4">
                <h3 class="text-xl font-semibold mb-2">Jane Smith</h3>
                <p class="text-gray-600">Head of Communications</p>
            </div>
            <div class="bg-white shadow-md rounded-lg p-6 text-center">
                <img src="https://via.placeholder.com/150" alt="Team Member 3" class="rounded-full mx-auto mb-4">
                <h3 class="text-xl font-semibold mb-2">Emily Johnson</h3>
                <p class="text-gray-600">Project Manager</p>
            </div>
        </div>
    </section>

</x-tenants-layout>
