<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Users') }}
        </h2>
    </x-slot>

    <div class="container mx-auto">
        <div class="py-12">
            <div class="w-full mx-auto sm:px-6 lg:px-8">
                <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="p-6 text-gray-900 dark:text-gray-100">

                        <a  class="inline-flex items-center px-4 py-2 bg-gray-800 dark:bg-gray-200 border border-transparent rounded-md font-semibold text-xs text-white dark:text-gray-800 uppercase tracking-widest hover:bg-gray-700 dark:hover:bg-white focus:bg-gray-700 dark:focus:bg-white active:bg-gray-900 dark:active:bg-gray-300 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 dark:focus:ring-offset-gray-800 transition ease-in-out duration-150'" href="{{ route('tenant.user.create') }}">
                            {{ __('Create User') }}
                        </a>
                    @if($users->count() > 0)
                            <table class="mx-auto text-center text-sm font-light text-surface dark:text-white">
                                <thead class="border-b border-neutral-200 font-medium dark:border-white/10">
                                <tr>
                                    <th class="px-6 py-">Name</th>
                                    <th class="px-6 py-">Email</th>
                                    <th class="px-6 py-">Company</th>
                                    <th class="px-6 py-">Role</th>
                                    <th class="px-6 py-">Team</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr class="border-b border-neutral-200 dark:border-white/10">
                                        <td class="whitespace-nowrap px-6 py-4">{{ $user->name }}</td>
                                        <td class="whitespace-nowrap px-6 py-4">{{ $user->emails }}</td>
                                        <td class="whitespace-nowrap px-6 py-4">{{ $user->tenant_id }}</td>
                                        <td class="whitespace-nowrap px-6 py-4">

                                            <label class="flex justify-between items-center p-2 text-xl">
                                                @php $role =  $user->is_admin? 'Admin ' : 'User';@endphp
                                                <span class="text-gray-700 dark:text-gray-300 mr-2">{{ $role }}</span>
                                                <input type="checkbox"  name="is_admin"  value="0"  class="appearance-none" {{ $user->is_admin ? 'checked' : '' }}    />
                                                <span class="w-16 h-10 flex items-center flex-shrink-0 ml-4 p-1 bg-gray-300 rounded-full after:w-8 after:h-8 after:bg-white after:rounded-full after:shadow-md"></span>
                                            </label>
                                        </td>
                                        @if(!empty($user->team->name ))
                                          <td class="whitespace-nowrap px-6 py-4">{{ $user->team->name }}</td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <p class="mb-0">You're not part of any users</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>



<x-modal name="users" name="user created" >
    <form method="POST" action="{{ route('tenant.user.store') }}">
        @csrf

        <!-- Team -->

        <div class="mt-4">
            <x-select-input  name="team_id"
                             :options="[1 => '9-2233-72-036-854-775-808', 2 => '9-2233-72-036-854-775-808']"
                             :selected="old('salt')"
                             class="block mt-1 w-full"
            />

            <!-- Name -->
            <div>
                <x-input-label for="name" :value="__('Name')" />
                <x-text-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
                <x-input-error :messages="$errors->get('name')" class="mt-2" />
            </div>

            <!-- Email Address -->
            <div class="mt-4">
                <x-input-label for="email" :value="__('Email')" />
                <x-text-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('emails')" required autocomplete="username" />
                <x-input-error :messages="$errors->get('emails')" class="mt-2" />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-input-label for="password" :value="__('Password')" />

                <x-text-input id="password" class="block mt-1 w-full"
                              type="password"
                              name="password"
                              required autocomplete="new-password" />

                <x-input-error :messages="$errors->get('password')" class="mt-2" />
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                <x-input-label for="password_confirmation" :value="__('Confirm Password')" />

                <x-text-input id="password_confirmation" class="block mt-1 w-full"
                              type="password"
                              name="password_confirmation" required autocomplete="new-password" />

                <x-input-error :messages="$errors->get('password_confirmation')" class="mt-2" />
            </div>

            <div class="flex items-center justify-end mt-4">

                <x-primary-button class="ms-4">
                    {{ __('Register') }}
                </x-primary-button>
            </div>
        </div>
    </form>
</x-modal>
</x-app-layout>
