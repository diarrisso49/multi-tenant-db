<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create User') }}
        </h2>
    </x-slot>

    <div>
        <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
            <form method="POST" action="{{ route('tenant.user.store') }}">
                @csrf

                <!-- Name -->
                <div>
                    <x-input-label for="name" :value="__('Name')" />
                    <x-text-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
                    <x-input-error :messages="$errors->get('name')" class="mt-2" />
                </div>

                <!-- Firstname -->
                <div>
                    <x-input-label for="firstname" :value="__('Firstname')"/>
                    <x-text-input id="firstname" class="block mt-1 w-full" type="text" name="firstname"
                                  :value="old('firstname')" required autofocus autocomplete="name"/>
                    <x-input-error :messages="$errors->get('firstname')" class="mt-2"/>
                </div>

                <!-- Firstname -->
                <div>
                    <x-input-label for="position" :value="__('Position')"/>
                    <x-text-input id="position" class="block mt-1 w-full" type="text" name="position"
                                  :value="old('position')" required autofocus autocomplete="name"/>
                    <x-input-error :messages="$errors->get('position')" class="mt-2"/>
                </div>


                <!-- Email Address -->
                <div class="mt-4">
                    <x-input-label for="emails" :value="__('Email')" />
                    <x-text-input id="emails" class="block mt-1 w-full" type="email" name="emails" :value="old('emails')" required autocomplete="username" />
                    <x-input-error :messages="$errors->get('emails')" class="mt-2" />
                </div>

                <!-- Password -->
                <div class="mt-4">
                    <x-input-label for="password" :value="__('Password')" />

                    <x-text-input id="password" class="block mt-1 w-full"
                                  type="password"
                                  name="password"
                                  required autocomplete="new-password" />

                    <x-input-error :messages="$errors->get('password')" class="mt-2" />
                </div>

                <!-- Confirm Password -->
                <div class="mt-4">
                    <x-input-label for="password_confirmation" :value="__('Confirm Password')" />

                    <x-text-input id="password_confirmation" class="block mt-1 w-full"
                                  type="password"
                                  name="password_confirmation" required autocomplete="new-password" />

                    <x-input-error :messages="$errors->get('password_confirmation')" class="mt-2" />
                </div>

                <div class="flex items-center justify-end mt-4">

                    <x-primary-button class="ms-4">
                        {{ __('Register') }}
                    </x-primary-button>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>

