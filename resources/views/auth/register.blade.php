<x-guest-layout>
    <form method="POST" action="/register">
        @csrf

        <!-- Comapany  -->
        <div>
            <x-input-label for="company" :value="__('Company')" />
            <x-text-input id="company" class="block mt-1 w-full" type="text" name="company" :value="old('company')" required  />
            <x-input-error :messages="$errors->get('company')" class="mt-2" />
        </div>


        <!-- SubDomain  -->
        <div class="mt-2">
            <x-input-label for="domain" :value="__('Domain')" />

            <div class="flex items-baseline">
                <x-text-input id="domain" class="block mt-1 mr-2 w-full" type="text" name="domain" :value="old('domain')" required
                />
                .{{ config('tenancy.central_domains')[0] }}

            </div>
            <x-input-error :messages="$errors->get('domain')" class="mt-2" />
        </div>


        <!-- Name -->
        <div>
            <x-input-label for="name" :value="__('Name')" />
            <x-text-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
            <x-input-error :messages="$errors->get('name')" class="mt-2" />
        </div>


        <!-- Firstname -->
        <div>
            <x-input-label for="firstname" :value="__('Firstname')"/>
            <x-text-input id="firstname" class="block mt-1 w-full" type="text" name="firstname"
                          :value="old('firstname')" required autofocus autocomplete="name"/>
            <x-input-error :messages="$errors->get('firstname')" class="mt-2"/>
        </div>


        <!-- position -->
        <div>
            <x-input-label for="position" :value="__('Position')"/>
            <x-text-input id="position" class="block mt-1 w-full" type="text" name="position" :value="old('position')"
                          required autofocus autocomplete="name"/>
            <x-input-error :messages="$errors->get('position')" class="mt-2"/>
        </div>


        <!-- Email Address -->
        <div class="mt-4">
            <x-input-label for="email" :value="__('Email')" />
            <x-text-input id="emails" class="block mt-1 w-full" type="email" name="emails" :value="old('emails')" required autocomplete="username" />
            <x-input-error :messages="$errors->get('emails')" class="mt-2" />
        </div>

        <!-- Password -->
        <div class="mt-4">
            <x-input-label for="password" :value="__('Password')" />

            <x-text-input id="password" class="block mt-1 w-full"
                          type="password"
                          name="password"
                          required autocomplete="new-password" />

            <x-input-error :messages="$errors->get('password')" class="mt-2" />
        </div>

        <!-- Confirm Password -->
        <div class="mt-4">
            <x-input-label for="password_confirmation" :value="__('Confirm Password')" />

            <x-text-input id="password_confirmation" class="block mt-1 w-full"
                          type="password"
                          name="password_confirmation" required autocomplete="new-password" />

            <x-input-error :messages="$errors->get('password_confirmation')" class="mt-2" />
        </div>

        <div class="flex items-center justify-end mt-4">
            <a class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" href="/login">
                {{ __('Already registered?') }}
            </a>

            <x-primary-button class="ms-4">
                {{ __('Register') }}
            </x-primary-button>
        </div>
    </form>
</x-guest-layout>
