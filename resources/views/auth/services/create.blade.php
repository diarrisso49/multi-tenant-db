<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create Service') }}
        </h2>
    </x-slot>


    <div>

        <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-white mt-5 mb-5">
                {{ __('Create Service') }}
            </h2>

            <form method="POST" action="{{ route('tenant.service.store') }}">
                @csrf
                <!-- Teams -->
                <div class="mb-4">
                    <x-input-label for="team_id" :value="__('Choose team')"/>
                    <x-select-input
                        name="team_id"
                        :options="$teams"
                        :selected="old('team_id')"
                        class="block mt-1 w-full"
                    />
                    <x-input-error class="mt-2" :messages="$errors->get('team_id')"/>
                </div>

                <!-- Department -->

                <div class="mb-4">
                    <x-input-label for="departement_id" :value="__('Choose departement')"/>
                    <x-select-input
                        name="departement_id"
                        :options="$departements"
                        :selected="old('departement_id')"
                        class="block mt-1 w-full"
                    />
                    <x-input-error class="mt-2" :messages="$errors->get('departement_id')"/>
                </div>

                <!-- Name -->
                <div>
                    <x-input-label for="name" :value="__('Name')" />
                    <x-text-input
                        id="name"
                        class="block mt-1 w-full"
                        type="text" name="name"
                        :value="old('name')"
                        required autofocus autocomplete="name"
                    />
                    <x-input-error :messages="$errors->get('name')" class="mt-2" />
                </div>

                <!-- description -->
                <div>
                    <x-input-label for="description" :value="__('description')" />
                    <x-text-input
                        id="description"
                        class="block mt-1 w-full"
                        type="text"
                        name="description" :value="old('description')"
                        required autofocus autocomplete="name"
                    />
                    <x-input-error :messages="$errors->get('description')" class="mt-2" />
                </div>

                <!-- Manager -->
                <div>
                    <x-input-label for="manager" :value="__('Manager')" />
                    <x-text-input
                        id="manager"
                        class="block mt-1 w-full"
                        type="text"
                        name="manager"
                        :value="old('manager')"
                        required autofocus autocomplete="name"
                    />
                    <x-input-error :messages="$errors->get('manager')" class="mt-2" />
                </div>

                <div class="flex items-center justify-end mt-4">

                    <x-primary-button class="ms-4">
                        {{ __('Save') }}
                    </x-primary-button>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>

