@extends('layouts.app')

@section('content')
    <h1>Créer un nouvel utilisateur</h1>
    <form action="{{ route('tenant.user.store') }}" method="POST">
        @csrf
        <div>
            <label for="name">Nom :</label>
            <input type="text" id="name" name="name" required>
        </div>
        <div>
            <label for="email">Email :</label>
            <input type="email" id="email" name="email" required>
        </div>
        <div>
            <label for="password">Mot de passe :</label>
            <input type="password" id="password" name="password" required>
        </div>
        <button type="submit">Enregistrer</button>
    </form>
@endsection
