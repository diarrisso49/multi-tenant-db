@extends('layouts.app')

@section('content')
    <h1>Détails de l'utilisateur</h1>
    <p>Nom : {{ $user->name }}</p>
    <p>Email : {{ $user->email }}</p>
    <a href="{{ route('tenant.user.index') }}">Retour à la liste</a>
@endsection
