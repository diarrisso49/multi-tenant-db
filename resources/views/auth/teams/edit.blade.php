

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Edite') ."Team: " .  $team->name }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
<section  class="container w-full">

    <form method="post" action="{{ route('tenant.teams.update', $team) }}" class="mt-6 space-y-6">
        @csrf
        @method('patch')

        <div>
            <x-input-label for="name" :value="__('Name')" />
            <x-text-input id="name" name="name" type="text" class="mt-1 block w-3/5"  :value="old('name', $team->name)" required  />
            <x-input-error class="mt-2" :messages="$errors->get('name')" />
        </div>

        <div>
            <x-input-label for="team_owner" :value="__('Team Owner')" />
            <x-text-input id="team_owner" name="team_owner" type="text" class="mt-1 block w-3/5" :value="old('team_owner', $team->team_owner)" required  />
            <x-input-error class="mt-2" :messages="$errors->get('team_owner')" />
        </div>

        <div>
            <x-input-label for="company" :value="__('Company')" />
            <x-text-input id="company" name="company" type="text" class="mt-1 block w-3/5" :value="old('company', $team->tenant_id)" required  />
            <x-input-error class="mt-2" :messages="$errors->get('company')" />
        </div>

        <div>
            <x-input-label for="personal_team" :value="__('Personal Team')" />
            <x-text-input id="personal_team" name="personal_team" type="text" class="mt-1 block w-3/5" :value="old('personal_team', $team->personal_team ? ' Private': 'Personal')" required  />
            <x-input-error class="mt-2" :messages="$errors->get('personal_team')" />
        </div>

        <div class="flex items-center gap-4">
            <x-primary-button>{{ __('Save') }}</x-primary-button>
        </div>
    </form>
</section>
        </div>
    </div>

</x-app-layout>
