<x-app-layout>

    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Team ') }}
        </h2>
    </x-slot>

    <div>
        <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
            <form method="POST" action="{{ route('tenant.teams.store') }}">
                @csrf



                <!-- Department  -->

                <div class="mt-4">
                    <x-select-input name="departement_id"
                                    :options="$departements"
                                    :selected="old('departement_id')"
                                    class="block mt-1 w-full"
                    />
                    <x-input-error :messages="$errors->get('departement_id')" class="mt-2"/>

                </div>


                <!-- Personal Team -->

                <div class="mt-4">
                    <x-input-label for="personal_team" :value="__('Personal Team')"/>
                    <x-select-input  name="personal_team"
                     :options="['0' => 'no' , '1' => 'yes']"
                     :selected="old('personal_team')"
                     class="block mt-1 w-full"
                    />
                    <x-input-error :messages="$errors->get('personal_team')" class="mt-2" />

                </div>

                <!-- Team Name -->
                <div>
                    <x-input-label for="name" :value="__('Team Name')" />
                    <x-text-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
                    <x-input-error :messages="$errors->get('name')" class="mt-2" />
                </div>

                <!-- Team Owner -->
                <div class="mt-4">
                    <x-input-label for="team_owner" :value="__('Team Owner')" />
                    <x-text-input id="team_owner" class="block mt-1 w-full" type="text" name="team_owner" :value="old('team_owner')"  />
                    <x-input-error :messages="$errors->get('team_owner')" class="mt-2" />
                </div>


                <div class="flex items-center justify-end mt-4">
                    <x-primary-button class="ms-4">
                        {{ __('Create') }}
                    </x-primary-button>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>
