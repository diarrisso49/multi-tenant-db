<x-app-layout>

    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Teams') }}
        </h2>
    </x-slot>


    <div class="py-12">
        <div class="w-full  mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="p-6 sm:px-20 bg-white border-b border-gray-200">
                    <div class="flex justify-between">
                        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                            {{ __('Teams') }}
                        </h2>
                        <x-link-button href="{{ route('tenant.teams.create')}}">
                            {{ __('Create Team') }}
                        </x-link-button>
                    </div>
                </div>
                <div class="p-6 sm:px-20 bg-white border-b border-gray-200">
                    <table class="min-w-full divide-y divide-gray-200">
                        <thead>
                            <tr>
                                <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                    {{ __('Id') }}
                                </th>

                                <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                    {{ __('Name') }}
                                </th>
                                <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                    {{ __('Team  Owner') }}
                                </th>

                                <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                    {{ __('Company') }}
                                </th>

                                <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                    {{ __('personal_team') }}
                                </th>
                                <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">

                                    {{ __('Private') }}
                                </th>

                                <th class="px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                                    {{ __('Action') }}
                                </th>
                            </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">
                            @foreach ($teams as $team)
                                <tr>
                                    <td class="px-6 py-4 whitespace-no-wrap">{{ $team->id }}</td>
                                    <td class="px-6 py-4 whitespace-no-wrap">{{ $team->name }}</td>
                                    <td class="px-6 py-4 whitespace-no-wrap">{{ $team->team_owner }}</td>
                                    <td class="px-6 py-4 whitespace-no-wrap">{{ $team->tenant_id }}</td>
                                    @php
                                        $team->tenant_personal_team = $team->tenant_personal_team ? 'Personal Team' : 'Private Team';
                                        $team->private = $team->private ? 'Private Team' : 'Personal Team';
                                    @endphp
                                    <td class="px-6 py-4 whitespace-no-wrap">{{ $team->tenant_personal_team }}</td>
                                    <td class="px-6 py-4 whitespace-no-wrap">{{ $team->private }}</td>
                                    <td class="px-6 py-4 whitespace-no-wrap">
                                        <div class="flex items-center gap-4">
                                        <x-link-button href="{{ route('tenant.teams.edit', $team->id) }}">
                                            {{ __('Edit') }}
                                        </x-link-button>
                                        <form action="{{ route('tenant.teams.destroy', $team->id) }}" method="POST" class="delete-team-form">
                                            @csrf
                                            @method('DELETE')
                                            <x-secondary-button type="submit" onclick="return confirm('Are you sure you want to delete this usere?');">
                                                {{ __('Delete') }}
                                            </x-secondary-button>
                                        </form>
                                            </div>


                                </tr>

                                <ul class="mt-4 space-y-4">
                                    @foreach($team->users as $user)
                                        <li class="flex items-center justify-between p-4 bg-white dark:bg-gray-800 rounded-lg shadow">
                                            <div>
                                                <h4 class="text-md font-medium text-gray-900 dark:text-gray-100">{{ $user->name }}</h4>
                                                <p class="text-sm text-gray-500 dark:text-gray-400">{{ $user->emails }}</p>
                                            </div>
                                            <div class="flex items-center space-x-2">
                                                @if($user->pivot->role === 'admin')
                                                    <span class="px-2 py-1 text-xs font-semibold text-white bg-green-500 rounded-full">Admin</span>
                                                @else
                                                    <span class="px-2 py-1 text-xs font-semibold text-gray-700 dark:text-gray-300 bg-gray-100 dark:bg-gray-700 rounded-full">Member</span>
                                                @endif
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="p-6 sm:px-20 bg-white border-b border-gray-200">

                    <form action="{{ route('tenant.teams.invite', 1) }}" method="POST">
                        @csrf
                        <div class="mb-4">
                            <x-input-label for="team_id" :value="__('Choose Team')" />
                            <x-select-input
                                 name="team_id"
                                 :options=$selectedTeams
                                 :selected="old('team_id')"
                                 class="block mt-1 w-full"
                            />
                            <x-input-error class="mt-2" :messages="$errors->get('team_id')" />
                        </div>
                        <div class="mb-4">
                            <x-input-label for="emails" :value="__('Add Users')" />
                            <x-text-input id="emails" name="emails" type="email" class="mt-1 block w-full" :value="old('emails')" required />
                            <x-input-error class="mt-2" :messages="$errors->get('emails')" />
                        </div>
                        <x-primary-button class="ms-3">
                            {{ __('Add') }}
                        </x-primary-button>
                    </form>
                </div>

            </div>

        </div>

    </div>

</x-app-layout>
