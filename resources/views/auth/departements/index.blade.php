<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Department') }}
        </h2>
    </x-slot>

    <div class="container mx-auto">
        <div class="py-12">
            <div class="w-full mx-auto sm:px-6 lg:px-8">
                <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="p-6 text-gray-900 dark:text-gray-100">

                        <a class="inline-flex items-center px-4 py-2 bg-gray-800 dark:bg-gray-200 border border-transparent rounded-md font-semibold text-xs text-white dark:text-gray-800 uppercase tracking-widest hover:bg-gray-700 dark:hover:bg-white focus:bg-gray-700 dark:focus:bg-white active:bg-gray-900 dark:active:bg-gray-300 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 dark:focus:ring-offset-gray-800 transition ease-in-out duration-150'"
                           href="{{ route('tenant.departement.create') }}">
                            {{ __('Create Department') }}
                        </a>
                        @if($departements->count() > 0)
                            <table class="mx-auto text-center text-sm font-light text-surface dark:text-white">
                                <thead class="border-b border-neutral-200 font-medium dark:border-white/10">
                                <tr>
                                    <th class="px-6 py-">Name</th>
                                    <th class="px-6 py-">Description</th>
                                    <th class="px-6 py-">Name</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($departements as $departement)
                                    <tr class="border-b border-neutral-200 dark:border-white/10">
                                        <td class="whitespace-nowrap px-6 py-4">{{ $departement->name }}</td>
                                        <td class="whitespace-nowrap px-6 py-4">{{ $departement->description }}</td>
                                        <td class="whitespace-nowrap px-6 py-4">{{ $departement->manager }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <p class="mb-0">You're not part of any departements</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


    <x-modal name="Departement" name="Departement created">
        <form method="POST" action="{{ route('tenant.departement.store') }}">
            @csrf
            <!-- Team -->
            <div class="mt-4">
                <!-- Name -->
                <div>
                    <x-input-label for="name" :value="__('Name')"/>
                    <x-text-input
                        id="name"
                        class="block mt-1 w-full"
                        type="text"
                        name="name"
                        :value="old('name')"
                        required autofocus autocomplete="name"
                    />
                    <x-input-error :messages="$errors->get('name')" class="mt-2"/>
                </div>

                <!-- Description -->
                <div class="mt-4">
                    <x-input-label for="description" :value="__('Description')"/>
                    <x-text-input
                        id="description"
                        class="block mt-1 w-full"
                        type="description"
                        name="description"
                        :value="old('description')"
                        required autocomplete="description"
                    />
                    <x-input-error :messages="$errors->get('description')" class="mt-2"/>
                </div>

                <!-- Manager -->
                <div class="mt-4">
                    <x-input-label for="manager" :value="__('Manager')"/>

                    <x-text-input
                        id="manager"
                        class="block mt-1 w-full"
                        name="manager"
                        required autocomplete="manager"/>
                    <x-input-error :messages="$errors->get('manager')" class="mt-2"/>
                </div>

                <div class="flex items-center justify-end mt-4">
                    <x-primary-button class="ms-4">
                        {{ __('Save') }}
                    </x-primary-button>
                </div>
            </div>
        </form>
    </x-modal>
</x-app-layout>
