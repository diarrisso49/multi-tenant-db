document.addEventListener('DOMContentLoaded', function () {
    const teamSelect = document.getElementById('team_id');
    const inviteForm = document.getElementById('inviteForm');

    teamSelect.addEventListener('change', function () {
        const selectedTeamId = this.value;
        const route = "{{ route('tenant.teams.invite', ':teamId') }}";

        inviteForm.action = route.replace(':teamId', selectedTeamId);
    });


    teamSelect.dispatchEvent(new Event('change'));
});


setTimeout(function () {
    const flashMessage = document.getElementById('success');

    if (flashMessage) {
        flashMessage.style.display = 'none';
    }
}, 5000);
